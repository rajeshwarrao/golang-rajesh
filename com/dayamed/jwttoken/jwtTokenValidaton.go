package jwttoken

import(
	"net/http"
	structs "../structs"
	"github.com/dgrijalva/jwt-go"
	"fmt"
)

func JwtTokenValidation(r *http.Request)  *structs.Response {
	jwtToken := r.Header.Get("Authorization")
	fmt.Println("jwtToken=======>>>", jwtToken)
	
	reponse := &structs.Response{Status:"success",Message:"success"}
	if jwtToken == ""  {
			return &structs.Response{Status:"Fail",Message:"JWT Token is empty"}
	}

	// Initialize a new instance of `Claims`
	claims := &structs.Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(jwtToken, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return &structs.Response{Status:"Fail",Message:"JWT Signature Invalid"}
		}
		return &structs.Response{Status:"Fail",Message:"JWT Token not Valid"}
	}
	if !tkn.Valid {
		return &structs.Response{Status:"Fail",Message:"Unauthorized token"}
	}

	// Finally, return the welcome message to the user, along with their
	// username given in the token
	return reponse;
}