package jwttoken

import (
	"time"
	functions "../structs"
	"github.com/dgrijalva/jwt-go"

)

var jwtKey = []byte("SenecaGlobal")

func JwtGenerater(creds functions.Credentials) string {
	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(5 * time.Minute)	
	// Create the JWT claims, which includes the username and expiry time
	claims := &functions.Claims{
		Username: creds.Username,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	
		// Declare the token with the algorithm used for signing, and the claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		// Create the JWT string
		tokenString, err := token.SignedString(jwtKey)

		if err != nil {
			// If there is an error in creating the JWT return an internal server error
			return "error"
		}
		return tokenString
}


func JwtGeneraterWithUserDetails(userdetails functions.UserDetails) string {
	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(5 * time.Minute)	
	// Create the JWT claims, which includes the username and expiry time
	claims := &functions.Claims{
		Username: userdetails.First_name,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	
		// Declare the token with the algorithm used for signing, and the claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		// Create the JWT string
		tokenString, err := token.SignedString(jwtKey)

		if err != nil {
			// If there is an error in creating the JWT return an internal server error
			return "error"
		}
		return tokenString
}