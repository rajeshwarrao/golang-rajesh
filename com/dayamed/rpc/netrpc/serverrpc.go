package netrpc

import (
	"net/rpc"
	"fmt"
	"net"
)

type Listener int

func (l *Listener) GetLine(line []byte, reply *Reply) error  {
	rv := string(line)
	fmt.Printf("Receive: %v\n", rv)
	*reply = Reply{rv}
	return nil
}

func ServerRpc()  {
	fmt.Println("server rpc started.....")
	addr, err := net.ResolveTCPAddr("tcp", "0.0.0.0:12121")
	if err != nil {
		panic(err)
	}
	inbound, err := net.ListenTCP("tcp", addr)
   if err != nil {
    panic(err)
   }
	listener := new(Listener)
	rpc.Register(listener)
	rpc.Accept(inbound)
}