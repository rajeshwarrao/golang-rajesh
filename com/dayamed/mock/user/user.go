package user

import  doer "../doer"

type User struct {
	Doer doer.Doer
}

func (u *User) User() error {
	return u.Doer.Dosomething(12, "Hello")
}