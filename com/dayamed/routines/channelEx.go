package routines


import (
	"fmt"
	"time"
	structs "../structs"
	"encoding/json"
	"sync"
	"github.com/enriquebris/goconcurrentqueue"
	"net/smtp"

)
/*
func ChannelTest()  {
	t := time.Now()
	var c chan int = make(chan int)
	for i := 0 ; i<10 ; i++ {
	
	go RotuineFun(c, i)
	//time.Sleep(time.Second * 2)
	
	}
	//close(c)
	msg := <- c
	/* for ch := range c {
		fmt.Println("===cfff=ch====", ch)
	} 
	fmt.Println("loop function end", msg)
	diff := time.Now().Sub(t).Seconds()
	fmt.Println("difference and end time ", diff, time.Now())
}

func RotuineFun(c chan int, i int){
	fmt.Println("process time start ...",i , time.Now())
	time.Sleep(time.Second * 1)
	//msg := <- c
	c <- i
	//c <- fmt.Sprintf("%s %d",msg, i)
	//c <- fmt.Sprintf("%s, %s", msg, i)
	//close(c)
}
*/

var (
	queue2 = goconcurrentqueue.NewFIFO()
)

func ChannelPayrll()  {
	fixedRoutineCount := 50
	sendMessageCount := 3
	var wg sync.WaitGroup
	//loading into the queue
	for i := 0; i< sendMessageCount; i++ {
		message := &structs.Message{Id: i, Message:"messsage test",Desc:"test",ToEmail:"raja.kodati2020@gmail.com",Status:"NA"};
		//fmt.Println(" message.......", message)
		queue2.Enqueue(message);
	}
	startTime := time.Now()
	fmt.Println(" startTime .......", startTime)
	var results []structs.Message
	ch := make(chan structs.Message)
	//check the queue size and define routine count
	for queue2.GetLen()>0  {
		routineCount := queue2.GetLen() 
		if(routineCount > fixedRoutineCount){
			routineCount = fixedRoutineCount
		}
		//fmt.Println("===queue size=======>>",queue2.GetLen())
		//initate the routines.
		for j :=0 ; j<routineCount ; j++ {
			wg.Add(1)
			msg, _ := queue2.DequeueOrWaitForNextElement()
			message := getMessageStrut(msg)
			//fmt.Println(" message .......", message)
			go SendNotificationUpdate(ch, message, &wg)
	}

	go func() {
        for v := range ch {
			fmt.Println("===result mesage size===%%%%%==",v)
            results = append(results, v)
        }
	}()
	//close(ch)
		wg.Wait()
	}
	diff := time.Now().Sub(startTime).Seconds()
	fmt.Println("End main Method....===== end time  ",diff, time.Now())
}


func getMessageStrut(msg interface{}) structs.Message {
    message := structs.Message{}
    bodyBytes, _ := json.Marshal(msg)
    json.Unmarshal(bodyBytes, &message)
    return message
}


func SendNotificationUpdate(message chan structs.Message, msg structs.Message, wg *sync.WaitGroup){
	fmt.Println(" send mail start time .......", time.Now())
	msg.Status = "P"
	defer wg.Done()
	from := msg.ToEmail
	pass := "Raja2020@"
	to := msg.ToEmail
	//fmt.Println(" To mail and from mail===", to, from)
	mailmsg := "From: " + from + "\n" +
	"To: " + to + "\n" +
	"Subject: Hello there\n\n" +
	"Body....." 
	err := smtp.SendMail("smtp.gmail.com:587",
	smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
	from, []string{to}, []byte(mailmsg))
	if err != nil {
		fmt.Println("error occured", err)
		msg.Status = "Error"
	}
	msg.Status = "Success"
	fmt.Println(" send mail end time .......", time.Now())
	message <- msg
}