package routines

import(
	"fmt"
	structs "../structs"
	jwttoken "../jwttoken"
	db "../db"
	"time"
	"encoding/json"
)

func GetUserwithJWTtokne(creds *structs.Credentials)  {
	fmt.Println("getUserwithJWTtokne==", creds.Username, creds.Password)
	userdetail := make(chan structs.UserDetails)
	loginResponse := make(chan structs.LoginResponse)
	//res := make(chan structs.LoginResponse)
	go GetUserDetailsFromDB(userdetail, *creds )
	go GetJWTToken(userdetail, loginResponse)
	res  := <- loginResponse
	fmt.Println(" Response.....", res)
	//fmt.Println(" Response in strutu....", getLoginResponsetrut(res))

}

func getLoginResponsetrut(msg interface{}) structs.LoginResponse {
    message := structs.LoginResponse{}
    bodyBytes, _ := json.Marshal(msg)
    json.Unmarshal(bodyBytes, &message)
    return message
}

func GetJWTToken(userDetails chan structs.UserDetails, response chan structs.LoginResponse){
	fmt.Println("GetJWTToken invoked and waiting resp....")
	user := <- userDetails
	fmt.Println("user object from go routine getUserDetailsFromDB...", user)
	var loginResponse structs.LoginResponse
	if user != (structs.UserDetails{}) {
		jwtTokenValue := jwttoken.JwtGeneraterWithUserDetails(user)
		loginResponse = structs.LoginResponse{Status: "Sucess", Message: "User details valid", JwtToken: jwtTokenValue, Username:  user.First_name }
		response  <- loginResponse
		}else{
		loginResponse = structs.LoginResponse{Status: "Fail", Message: "Invalid User details",  Username:  user.First_name}
		response  <- loginResponse
	}
}

func GetUserDetailsFromDB(userDetails chan structs.UserDetails, creds structs.Credentials){
	fmt.Println("getting userdetails from db...", creds)
	user  := db.GetUserDetailByEmailId(creds.Username)
	fmt.Println(" user object...sleeping.", user);
	time.Sleep(time.Second * 5)
	userDetails <- *user
}

func GoRoutineDepencyCheck(){
	creds :=  &structs.Credentials{Username:"a@d.com", Password:"rr"}
	GetUserwithJWTtokne(creds)
}