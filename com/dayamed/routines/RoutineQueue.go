package routines


import ( "github.com/enriquebris/goconcurrentqueue"
structs "../structs"
"fmt"
"sync"
"time"
)



var (
	queue = goconcurrentqueue.NewFIFO()
	done = make(chan struct{})
	routineQueue = goconcurrentqueue.NewFIFO()
)

func QueueInsert()  {
	startTime := time.Now()
	fixedRoutineCount := 100
	fmt.Println(" startTime .......", startTime)
	var wg sync.WaitGroup
	for i := 0; i<20000; i++ {
		message := &structs.Message{Id: i, Message:"messsage=="+string(i),Desc:"test"};
		//fmt.Println(" message.......", message)
		queue.Enqueue(message);
	}
	
	for queue.GetLen()>0  {
		routineCount := queue.GetLen() 
		if(routineCount > fixedRoutineCount){
			routineCount = fixedRoutineCount
		}
		//fmt.Println("===queue size===",queue.GetLen())
		//fmt.Println("===routineCount===",routineCount)
		for j :=0 ; j<routineCount ; j++ {
			wg.Add(1)
			go GoRoutine(&wg)
		}
		wg.Wait()
	}
	diff := time.Now().Sub(startTime).Seconds()
		fmt.Println("End main Method....===== end time  ",diff, time.Now())
}

func GoRoutine(wg *sync.WaitGroup)  {
	message, _ := queue.DequeueOrWaitForNextElement()
	//fmt.Println("====message data",message )
	routineQueue.Enqueue(message)
	//sendNotification(message)
	time.Sleep(time.Millisecond * 200)
	defer wg.Done()
}

func sendNotification(message  structs.Message){

}