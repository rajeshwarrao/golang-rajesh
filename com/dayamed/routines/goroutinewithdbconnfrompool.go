package routines

import (
	"fmt"
	db "../db"
	structs "../structs"
	"strconv"
	"sync"
)

func  GoroutineWithDbpool()  {
	fmt.Println("==GoroutineWithDbpool===")
	userDetails := make(chan structs.UserDetails)
	routineCount := 132
	var wg sync.WaitGroup
	
	//var userDetailsArra := make([]structs.UserDetails, routineCount)
	for i := 101; i< routineCount; i++ {

			if i%2 ==0 {
				wg.Add(1)
				email1_id := "p"+strconv.FormatInt(int64(i-20), 10)+"@p.com"
				fmt.Println("==email1_id=to get data==", email1_id)
				go db.GetUserDetails(userDetails,email1_id, &wg)
			}else{
				wg.Add(1)
				go InsertUserDetails(userDetails, &wg, strconv.FormatInt(int64(i), 10))
			}
	}
	go func() {
        for v := range userDetails {
			if (structs.UserDetails{}) != v  {
							fmt.Println("===userDetails arary=======",v)
			}
            //results = append(results, v)
        }
	}()
	wg.Wait()
}

func  InsertUserDetails(userDetails chan structs.UserDetails, wg *sync.WaitGroup, count string){
	i64, err := strconv.ParseInt(count, 10, 64)
	if err != nil {
		panic(err)
	}
intcount := int(i64)
userDetailsStrut := structs.UserDetails{User_id: intcount,Gender:"Male", Password:"1243",Role:"Patient",Email_id:"p"+count+"@p.com" ,First_name:"QQ"+count,Last_name:"lstname"+count,Mobilenumber:intcount, Id:intcount, Name:"name"+count}
db.SaveUserDetailsUtily(userDetails,userDetailsStrut, wg)
fmt.Println("userdetails saved successfully.......",userDetailsStrut.Email_id )

}