package error


type error interface {
	Error() string
}