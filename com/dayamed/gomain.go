package main

import (
	 "fmt"
	 "net/http"
	 "net/smtp"
	 api "./api"
	 db "./db"
	 "github.com/labstack/echo"
	 "github.com/labstack/echo/middleware"
	//refelction "./refelction"
	encryption "./encryption"
//concurrncy "./concurrncy"
	"github.com/gorilla/mux"
	"time"
	"errors"
	jwttoken "./jwttoken"
	"golang.org/x/text/message"
 "golang.org/x/text/language"
 _ "golang.org/x/text/message/catalog"
 logger "./logger"
 channelsAsync "./channelsAsync"
 v2 "./v2"
 routines "./routines"
 sendemail "./sendemail"
 netrpc "./rpc/netrpc"
 structs "./structs"
 "encoding/json"
)

func main(){
	//MultiLangTest()
	//encryption.ExampleNewGCMEncrypter()
	//sendemail()
	//EchoTestServer()
	//TestChannel()
	//V2Testing()
	//concurrncy.TestconcrryThread()
	//mainTest()
	//Goroutine()
	//SendEmail()
	//RepcServer()
	DBUtilyTest()
	
}

func RepcServer(){
	netrpc.ServerRpc()
}

func SendEmail(){
	sendemail.Sendemail()
}

func Goroutine()  {
	//routines.QueueInsert()
	//routines.ChannelPayrll()
	routines.GoroutineWithDbpool()
}

func DBUtilyTest(){
	mapOutPar := db.SelectStatmentQuery()
	fmt.Println("==map in main method======", mapOutPar)

	fmt.Println("==map in main method======", getMessageStrut(mapOutPar))
}

func getMessageStrut(msg interface{}) structs.UserDetails {
    message := structs.UserDetails{}
    bodyBytes, _ := json.Marshal(msg)
    json.Unmarshal(bodyBytes, &message)
    return message
}

func SignutreValue(){
	
}

func V2Testing(){
	fmt.Println("go running......");
	v2.Submain()
}

func TestChannel(){
	fmt.Println("going to call process")
	r := <- channelsAsync.ChannTest() 
	s := <- channelsAsync.ChannTest() 
	p := <- channelsAsync.ChannTest()
	fmt.Println("process completed")
	fmt.Println(r)
	fmt.Println(s)
	fmt.Println(p)
}

func sendemail_mainmethod() {
	// Choose auth method and set it up
	auth := smtp.PlainAuth("", "RAJESHWARRAO.KODATI@SENECAGLOBAL.COM", "Raja2019&", "192.168.1.20")
fmt.Println("sending email......")
	// Here we do it all: connect to our server, set up a message and send it
	to := []string{"RAJESHWARRAO.KODATI@SENECAGLOBAL.COM"}
	msg := []byte("To: RAJESHWARRAO.KODATI@SENECAGLOBAL.COM\r\n" +
		"Subject: Why are you not using Mailtrap yet?\r\n" +
		"\r\n" +
		"Here’s the space for our great sales pitch\r\n")
	err := smtp.SendMail("192.168.1.20:25", auth, "RAJESHWARRAO.KODATI@SENECAGLOBAL.COM", to, msg)
	
	if err != nil {
		panic(err)
	} else {
		fmt.Println("sending email success......")
	}
}

func MultiLangTest()  {
	p := message.NewPrinter(language.BritishEnglish)
 p.Printf("There are %v flowers in our garden.\n", 1500)
 
 p = message.NewPrinter(language.Japanese)
 p.Printf("There are %v flowers in our garden.", "Hello")
}

func RefelectionTest(){
	//refelction.Tsetcrete()
	fmt.Println("====server going to up")
	http.HandleFunc("/signin", api.Signin)
	http.HandleFunc("/jwtvalidation", api.JWTValidation)
	http.HandleFunc("/getAllPatients", api.GetAllPatientsDetails)
	http.HandleFunc("/saveUser", api.SaveUserDetails)
	http.ListenAndServe(":8081", nil)
	fmt.Println("====server up now ") 
	encryption.ExampleNewGCMEncrypter()
	//encryption.ExampleNewGCMDecrypter()
	//encryption.ExampleNewGCM_decryptNew()
	//concurrncy.Testconcrry()

}

func GorillaMuxTest(){
	//Gorilla Mux example
	 r := mux.NewRouter();
	s := r.PathPrefix("/rest").Subrouter();
	s.HandleFunc("/jwtvalidation", api.JWTValidation)
	s.HandleFunc("/getAllPatients", api.GetAllPatientsDetails)
	s.HandleFunc("/saveUser", api.SaveUserDetails)
	r.HandleFunc("/signin", api.Signin)
	serve := &http.Server{
		Handler : r,
		Addr : "192.168.8.146:8081",
		WriteTimeout: 15 * time.Second,
        ReadTimeout:  15 * time.Second,
	}
	fmt.Println("server started.....11....")
	serve.ListenAndServe()
	fmt.Println("server started.........") 
}


func EchoTestServer(){
	//Echo example
	
	e := echo.New()
	//e.Use(middleware.Logger())
	//e.Use(middleware.Recover())
	//e.Use(JWTValidation)
	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	   }))

	e.POST("/login", api.Login)
	e.GET("/getPatients", api.GetPatients)
	e.GET("/ws/:username", api.HandleConnections)
	 e.GET("/callws/:username", api.WebScoketTest)  
	 e.GET("/getliveusers", api.GetLiveUsers)
	 e.POST("/uploadfile", api.UploadFile)
	fmt.Println("server going start.......")
	fmt.Println("initiating logs.......")
	logger.Init()
	//WebSocketListen()
	e.Logger.Fatal(e.Start("192.168.8.146:8081"))   
	
}

func WebSocketListen(){
	//go handleMessages()
}

func JWTValidation(next echo.HandlerFunc) echo.HandlerFunc  {

	return func (c echo.Context) error {
		fmt.Println("======calling middleware=========", c.Request().URL.Path)
		if c.Request().URL.Path == "/login"{ 
			fmt.Println("===create JWT token===$$$$$$$$$$$")
			return next(c)
		}else{
			fmt.Println("===validate JWT token===@@@@@@")
			response := jwttoken.JwtTokenValidation(c.Request());
			if response.Status == "success"{
				return next(c)
			}else{
				return echo.NewHTTPError(http.StatusInternalServerError, errors.New("JWT Token error"))
			}
		}
		
	}
	
}



