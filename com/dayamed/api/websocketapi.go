package api

import (
	"github.com/gorilla/websocket"
	"net/http"
	structs "../structs"
	"fmt"
	"github.com/labstack/echo"
	"net/url"
)

var clients =  make(map[string] *websocket.Conn)
var broadcast = make(chan structs.WebSocketMessage) 
// Configure the upgrader
var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}

func HandleConnections(c echo.Context) error{
	r := c.Request()
	
	username := c.Param("username")
	fmt.Println("web socket init login username===", username)
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	fmt.Println("web socket cehck origins")
	origin := r.Header["Origin"]
	if len(origin) == 0 {
		fmt.Println(" ording lent is 0")
	}
	u, err := url.Parse(origin[0])
	if err != nil {
		panic(err)
	}
	fmt.Println("web socket u host ", u.Host);
	fmt.Println("web socket r host ", r.Host);
	 // Upgrade initial GET request to a websocket
	 ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	 if err != nil {
			 panic(err)
	 }
	 clients[username] = ws
	 fmt.Println("web socket init done &&&&===", ws)
 // Make sure we close the connection when the function returns
	 //defer ws.Close()
	 return c.String(http.StatusOK, "success")
}

func WebScoketTest(c echo.Context) error{
	//r := c.Request()
	username := c.Param("username")
	message := &structs.WebSocketMessage{
		Message: "socketiniti",
		Status: "success",
		Sendid: "s@s.com",
		Recevierid: "r@r.com"}
		SendMsgWebsocketNotificaotn(username, *message)
		return c.String(http.StatusOK, "success")
}

func SendMsgWebsocketNotificaotn(username string, message structs.WebSocketMessage)  {
	fmt.Println("web socket calling username^^^^===", username)
	ws := clients[username]
	err := ws.WriteJSON(message)
	if err != nil {
			panic(err)
	}
}
