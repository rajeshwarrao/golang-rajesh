package api

import (
		"encoding/json"
		"fmt"
		"net/http"
		structs "../structs"
		jwttoken "../jwttoken"
		db "../db"
		"github.com/labstack/echo"
		logger	"../logger"
)


func Signin(w http.ResponseWriter, r *http.Request) {
	var creds structs.Credentials
	 
	// Get the JSON body and decode into credentials
	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		// If the structure of the body is wrong, return an HTTP error
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	//checking username in database
	userDetails  := db.GetUserDetailByEmailId(creds.Username)
	fmt.Println("=userDetails return from do layer ===>>===",userDetails)
	if (structs.UserDetails{}) == *userDetails {
		loginResponse := &structs.LoginResponse{Status: "Fail", Message: "Invalid User details" }
		userJson, err := json.Marshal(loginResponse)

		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type","application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(userJson)

	}else{
		jwtTokenValue := jwttoken.JwtGenerater(creds)
		fmt.Println("===jwtToken===>>===",creds.Username, jwtTokenValue)

		if jwtTokenValue == "error" {
			// If there is an error in creating the JWT return an internal server error
			w.WriteHeader(http.StatusInternalServerError)
			panic("error while create JWT token")
			//return
		}
		loginResponse := &structs.LoginResponse{JwtToken: jwtTokenValue, Username:  creds.Username }
	
		userJson, err := json.Marshal(loginResponse)

		if err != nil {
			panic(err)
		}
		w.Header().Set("Content-Type","application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(userJson)
	}
}


	var credentialArray =  make(map[int] *structs.Credentials)
 var i int = 0;
func Login(c echo.Context) error{

	var creds structs.Credentials
		defer c.Request().Body.Close()
		err := json.NewDecoder(c.Request().Body).Decode(&creds)
		if err != nil {
			logger.ErrorLog.Printf("failed to load the request Object %s", err)
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error)
		}
	credentialArray[i]= &creds
	i++
	logger.InfoLog.Printf("loging success creds.Username ===>>=== >"+creds.Username)
	jwtTokenValue := jwttoken.JwtGenerater(creds)
	userDetails  := db.GetUserDetailByEmailId(creds.Username)
	fmt.Println("=userDetails return from do layer ===>>===",userDetails)
	if (structs.UserDetails{}) == *userDetails {
			loginResponse := &structs.LoginResponse{Status: "Fail", Message: "Invalid User details", JwtToken: jwtTokenValue, Username:  creds.Username }
			return c.JSON(http.StatusOK, loginResponse)
		}else {
			loginResponse := &structs.LoginResponse{JwtToken: jwtTokenValue, Username:  creds.Username }
			return c.JSON(http.StatusOK, loginResponse)
		}
}

func GetLiveUsers(c echo.Context) error  {
	return c.JSON(http.StatusOK, credentialArray)
}


