package api

import(
	"fmt"
	"net/http"
	"github.com/labstack/echo"
	"io/ioutil"
	"os"
	"github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"bytes"
	"github.com/aws/aws-sdk-go/aws/credentials" 
)

var uploadPath string = "c:/tempfiles"
func UploadFile(c echo.Context) error  {
	fmt.Printf("File upload invoke")
	r := c.Request()

	// Parse our multipart form, 10 << 20 specifies a maximum
    // upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("filekey")
	if err != nil {
		panic(err)
	}
	defer file.Close()
    fmt.Printf("Uploaded File: %+v\n", handler.Filename)
    fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)
	//fmt.Printf("File Size: %+v\n",)
	tempfile, err := os.Create(uploadPath+"/"+handler.Filename)
	if err != nil {
		panic(err)
	}
	defer tempfile.Close()
	 // read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	tempfile.Write(fileBytes)

	UploadS3Bucket(uploadPath+"/"+handler.Filename, handler.Filename)
	fmt.Printf("file Uploaded.......")
	return c.String(http.StatusOK, "Success")
}

const (
    S3_REGION = "ap-south-1"
    S3_BUCKET = "testsg-uploadbygolang"
)

func  UploadS3Bucket(fieldir string, fielname string) {
	fmt.Printf("file Uploaded AWS S3.......")
	creds := credentials.NewStaticCredentials("AKIAJBHSWPBUBJJCOH5Q", "hB5e7kGerVKDRVXjuR1mbBYjsG74dQsa3r2+xoaM", "")
	fmt.Printf("AWS  creds check.......")
	cfg := aws.NewConfig().WithRegion(S3_REGION).WithCredentials(creds)
	fmt.Printf("AWS  cfg check ^^.......")
	s, err := session.NewSession(cfg)
	fmt.Printf("AWS  session created.........")
    if err != nil {
        panic(err)
    }

    // Upload
     AddFileToS3(s, fieldir, fielname)
  
}

func  AddFileToS3(s *session.Session, fileDir string, filename string)   {
	  // Open the file for use
	  file, err := os.Open(fileDir)
	  if err != nil {
		  panic(err)
	  }
	  defer file.Close()
	  fmt.Println("file loading from local.....filename....",filename)
	  // Get file size and read the file content into a buffer
	  fileInfo, _ := file.Stat()
	  var size int64 = fileInfo.Size()
	  buffer := make([]byte, size)
	  file.Read(buffer)
	  fmt.Println("fileInfo loading local.........",fileDir)
	  // Config settings: this is where you choose the bucket, filename, content-type etc.
	  // of the file you're uploading.
	  _, err = s3.New(s).PutObject(&s3.PutObjectInput{
		  Bucket:               aws.String(S3_BUCKET),
		  Key:                  aws.String(filename),
		  ACL:                  aws.String("public-read"),
		  Body:                 bytes.NewReader(buffer),
		  ContentLength:        aws.Int64(size),
		  ContentType:          aws.String(http.DetectContentType(buffer)),
		  ContentDisposition:   aws.String("attachment"),
		  //ServerSideEncryption: aws.String("AES256"),
	  })

	  fmt.Println("fileInfo uploaded success ****.........")
	 // return "sucsss"
}