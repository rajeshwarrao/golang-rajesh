package api

import (
	"net/http"
	db "../db"
	//"fmt"
	"encoding/json"
	encryption "../encryption"
	structs "../structs"
	"github.com/labstack/echo"
)

func GetAllPatientsDetails(w http.ResponseWriter, r *http.Request) {
	//if JWTValidationRes(w,r) {
		jwtToken := r.Header.Get("Authorization")
		userDetails  := db.GetAllPatients()
		encrypUserDetails := encryption.EncryptUserDetailsArray(*userDetails, jwtToken)
		userJson, err := json.Marshal(encrypUserDetails)

			if err != nil {
				panic(err)
			}
			w.Header().Set("Content-Type","application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(userJson)
	//}
}

func GetPatients(c echo.Context)  error{
	 encryptUserDetailsArray :=  []structs.UserDetails{ structs.UserDetails{User_id: 2, Gender: "3dT4qATArMHgtyY41_5I7_RgqmpXyw==",Password: "rpUv4r_LtGT8Fp0K-vPfqjoDvg==",
	Role: "yiKNA6wnKlt9WonSFgjZ87xP5mJh_fs=",Email_id: "LxeR3xo-QWgmifvRsrblhB5klkObc6mA",First_name: "sSVgxx7zq1oOPMSNpbANpf3Y",
	Last_name: "7mA2Kit7elvl0skoZAYDRPxT",Mobilenumber: 85, Id: 6, Name: "ABCCDD",Address: "Kraiminakak"}, structs.UserDetails{User_id: 5, Gender: "1223dT4qATArMHgtyY41_5I7_RgqmpXyw==",Password: "rpUv4r_LtGT8Fp0K-vPfqjoDvg==",
	Role: "yiKNA6wnKlt9WonSFgjZ87xP5mJh_f111s=",Email_id: "111LxeR3xo-QWgmifvRsrblhB5klkObc6mA",First_name: "111sSVgxx7zq1oOPMSNpbANpf3Y",
	Last_name: "7mA2Kit7elvl0skoZAYDRPxT",Mobilenumber: 123, Id: 4, Name: "Raejsh",Address: "Hyderadad"} }

	return c.JSON(http.StatusOK, encryptUserDetailsArray)
	
}

func SaveUserDetails(w http.ResponseWriter, r *http.Request) {
	//if JWTValidationRes(w,r) {
		jwtToken := r.Header.Get("Authorization")
				var userDetails structs.UserDetails
				// Get the JSON body and decode into credentials
				err := json.NewDecoder(r.Body).Decode(&userDetails)
				if err != nil {
					// If the structure of the body is wrong, return an HTTP error
					w.WriteHeader(http.StatusBadRequest)
					return
				}
			decryptUserDetails := encryption.DecryptUserDetails(&userDetails, jwtToken)
			db.SaveUserDetails(decryptUserDetails)
			loginResponse := &structs.LoginResponse{Status: "sucess", Message: "User details saved successfully" }
			userJson, err := json.Marshal(loginResponse) 
				if err != nil {
					panic(err)
				}
			w.Header().Set("Content-Type","application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(userJson)
	//}
}