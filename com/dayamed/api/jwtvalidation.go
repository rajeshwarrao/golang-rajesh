package api

import (
	"encoding/json"
		"net/http"
		jwttoken "../jwttoken"
)

func JWTValidation(w http.ResponseWriter, r *http.Request) {
	
	response := jwttoken.JwtTokenValidation(r)
	userResponse, err := json.Marshal(response)

	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type","application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userResponse)
}

func JWTValidationRes(w http.ResponseWriter, r *http.Request) bool {
	
	response := jwttoken.JwtTokenValidation(r)
	if response.Status == "success"{
		return true
	}
	userResponse, err := json.Marshal(response)

	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type","application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userResponse)
	return false
}

