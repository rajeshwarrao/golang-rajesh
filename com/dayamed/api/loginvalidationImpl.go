package api
import (
"net/http"
"github.com/labstack/echo"
)

type LoginValidation interface{
	Signin(w http.ResponseWriter, r *http.Request) 
	Login(c echo.Context) error
}