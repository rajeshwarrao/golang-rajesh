package structs

type Response struct {
	Status string
	Message string
}
