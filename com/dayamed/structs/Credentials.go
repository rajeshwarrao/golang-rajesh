package structs


// Create a struct that models the structure of a user, both in the request body, and in the DB
type Credentials struct {
	Password string  `json:"password" validate:"required,min=6,max=8"`
	Username string  `json:"username" validate:"required,email"`
}