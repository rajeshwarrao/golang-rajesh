package structs

type Message struct {
	Id int
	Message string
	Desc string
	ToEmail string
	Status string
}