package structs

type LoginResponse struct {
	JwtToken string
	Username string
	Status string
	Message string
}