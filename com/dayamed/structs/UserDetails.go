package structs

type UserDetails struct{
	User_id int
	Gender string
	Password string
	Role string
	Email_id string
	First_name string
	Last_name string
	Mobilenumber int
	Id int `json:"id"`
	Name string `json:"name"`
	Address  string `json:"address"`
}