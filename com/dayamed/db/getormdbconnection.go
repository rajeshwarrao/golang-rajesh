package db

import (
    "github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
    structs "../structs"
    "time"
)

//connection pool implemented
func GetORMDBConnection() (db*gorm.DB){
    // Initialise a new connection pool
    //db, err := sql.Open("postgres", "postgres://user:pass@localhost/db")
    db, err := gorm.Open("mysql", "root:root@/godb")

    //added with db host
    //db, err := gorm.Open("mysql", "root:root@(192.168.8.146:3306)/godb")

    //db, err := gorm.Open("mysql", "root:root@(192.168.1.86:3306)/godb?charset=utf8&parseTime=True&loc=Local")
    if err != nil {
        panic(err.Error())
     }
    // db.Exec("PRAGMA key = auxten;")
    // Migrate the schema
	db.AutoMigrate(&structs.UserDetails{})
    // SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
    db.DB().SetMaxIdleConns(15)

    // SetMaxOpenConns sets the maximum number of open connections to the database.
    db.DB().SetMaxOpenConns(25)

    // SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
    db.DB().SetConnMaxLifetime(5 * time.Second)

	return db
}