package db

import (
	structs "../structs"
)

func GetUserDetailByEmailId(emailId string) *structs.UserDetails {
	db := GetORMDBConnection()
	var  userDetails structs.UserDetails = structs.UserDetails{}
	db.Where(&structs.UserDetails{Email_id: emailId}).First(&userDetails)
	defer db.Close()
	return &userDetails
}

func GetAllPatients() *[]structs.UserDetails {
	db := GetORMDBConnection()
	res := []structs.UserDetails{}
	db.Find(&res)
	defer db.Close()
	return &res
}

func SaveUserDetails(userDetails *structs.UserDetails) *structs.UserDetails{
	db := GetORMDBConnection()
	db.Save(&userDetails)
	defer db.Close()
	return userDetails
}

