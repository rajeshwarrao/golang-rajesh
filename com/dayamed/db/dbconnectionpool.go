package db

import (
	"database/sql"
    "time"
    "github.com/jmoiron/sqlx"
)


//connection pool implemented
func GetDBConnectionFromPool() (db*sql.DB){
    // Initialise a new connection pool
    db, err := sql.Open("mysql","root:root@tcp(localhost:3306)/godb")

    //added with db host
    if err != nil {
        panic(err.Error())
     }
    //db.AutoMigrate(&structs.UserDetails{})
    // SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
    db.SetMaxIdleConns(15)

    // SetMaxOpenConns sets the maximum number of open connections to the database.
    db.SetMaxOpenConns(25)

    // SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
    db.SetConnMaxLifetime(5 * time.Second)
	return db
}



//connection pool implemented
func GetDBConnectionFromSQLXPool() (db*sqlx.DB){
    // Initialise a new connection pool
    //db, err := sql.Open("mysql","root:root@tcp(localhost:3306)/godb")
    db, err := sqlx.Open("mysql", "root:root@tcp(localhost:3306)/godb")
   

    //added with db host
    if err != nil {
        panic(err.Error())
     }
    //db.AutoMigrate(&structs.UserDetails{})
    // SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
    db.SetMaxIdleConns(15)

    // SetMaxOpenConns sets the maximum number of open connections to the database.
    db.SetMaxOpenConns(25)

    // SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
    db.SetConnMaxLifetime(5 * time.Second)
	return db
}