package db

import(
	structs "../structs"
	"database/sql"
	"fmt"
	"sync"
	//"github.com/jmoiron/sqlx"
	"encoding/json"
	//"reflect"
)




func GetUserDetails(userDetails chan structs.UserDetails, usermailid string, wg *sync.WaitGroup)   {
	db := GetDBConnectionFromPool()
	userDetailsStruct := structs.UserDetails{}
	//fmt.Println("==usermailid===", usermailid)
		
	//fmt.Println("get db connection from pool.....", db)
	defer wg.Done()
	
	stmt, err := db.Prepare("select user_id,gender,role,email_id,first_name,mobilenumber from user_details where email_id= ?")
	defer db.Close()
	if err != nil {
		//fmt.Println("stement error..",err)
		userDetails <- userDetailsStruct
	}
	
	err = stmt.QueryRow(usermailid).Scan(&userDetailsStruct.User_id,&userDetailsStruct.Gender,&userDetailsStruct.Role,&userDetailsStruct.Email_id,&userDetailsStruct.First_name,&userDetailsStruct.Mobilenumber)
	defer stmt.Close()
	if err != nil {
		//fmt.Println("error msg......",err)
		if err == sql.ErrNoRows {
				//fmt.Println("No Rows in result",err, usermailid)
				userDetails <- userDetailsStruct
			}else{
				//fmt.Println("userDetailsStruct ..",userDetailsStruct)
				userDetails <- userDetailsStruct
			}
	}
	userDetails <- userDetailsStruct
}



func SaveUserDetailsUtily(userDetails chan structs.UserDetails, userDetailsStruct structs.UserDetails, wg *sync.WaitGroup)   {
	db := GetDBConnectionFromPool()
	defer wg.Done()
	tx, err := db.Begin()
	defer db.Close()
	fmt.Println("saving userdetails .......",  userDetailsStruct.Email_id)
	stmt, err := tx.Prepare("insert into user_details (user_id,gender,password,role,email_id,first_name,last_name,mobilenumber,id,name) values( ?,?, ?,?,?,?,?,?,?,?)")
if err != nil {
	panic(err)
}
	defer stmt.Close()
defer tx.Rollback()
_, err = stmt.Exec(userDetailsStruct.User_id, userDetailsStruct.Gender, userDetailsStruct.Password,userDetailsStruct.Role,userDetailsStruct.Email_id,userDetailsStruct.First_name , userDetailsStruct.Last_name , userDetailsStruct.Mobilenumber, userDetailsStruct.Id, userDetailsStruct.Name )
	if err != nil {
		panic(err)
	}	
	err = tx.Commit()
	if err != nil {
		panic(err)
	}
	userDetails <- userDetailsStruct
}

func SelectStatmentQuery() *map[string]interface{}{
	query := "select user_id,gender,role,email_id,first_name,mobilenumber from user_details where email_id = :emailId" 
	inparammap := map[string]interface{}{"emailId": "admin8@p.com"}
	db := GetDBConnectionFromSQLXPool()
	rows, err := db.NamedQuery(query, inparammap)
	if err != nil {
		panic(err)
	}
	outmap := make(map[string]interface{})
	for rows.Next() {
		rows.MapScan(outmap)	
	}
	if true {
		// Convert byte arrays to strings
		for k, v := range outmap {
			if _, ok := v.([]byte); ok {
				// Damn. Byte. Arrays. Sqlx.
				outmap[k] = string(v.([]byte))
			}
		}
	}
	fmt.Println("=======rows==%%%====", outmap)
	return &outmap
}

func getMessageStrut(msg interface{}) structs.UserDetails {
    message := structs.UserDetails{}
    bodyBytes, _ := json.Marshal(msg)
    json.Unmarshal(bodyBytes, &message)
    return message
}