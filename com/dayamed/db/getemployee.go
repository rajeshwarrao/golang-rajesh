package db

import (
    "database/sql"
	
    _ "github.com/go-sql-driver/mysql"
)


import "fmt"


type Employee struct {
    Id    int
    Name  string
    City string
}


func getDbConnection() (db*sql.DB){
	
	dbDriver := "mysql"
	dbUser := "root"
	dbPassword := "root"
	dbName := "godb"
	//db, err := sql.open(dbDriver, dbUser+":"+dbPassword+"@/"+dbName)
	db, err := sql.Open(dbDriver, dbUser+":"+dbPassword+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func getEmployeeData(){
	db := getDbConnection()
	fmt.Println("==========",db)
	selqu, err := db.Query("select * from employee")
	if err != nil {
		panic(err.Error())
	}
	emp := Employee{}
	res := []Employee{}
	for selqu.Next() {
		var id int
		var name, city string
		selqu.Scan(&id, &name, &city)
		fmt.Println("employe details....",id, name)
		emp.Id = id
		emp.Name = name
		emp.City = city
		res = append(res,emp)
		defer db.Close()
	}
}

func main() {
   fmt.Println("Hello, World!")
   getEmployeeData()
   fmt.Println(" db ending.....")

} 

