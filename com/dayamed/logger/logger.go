package logger

import(
	"log"
	"os"
	//"go/build"
)
var InfoLog, ErrorLog *log.Logger 

func Init(){
	var basepath = "C:\\go_workspace\\logs"
	var infolog, infoerr = os.Create(basepath + "\\log_info.log")
	if infoerr != nil {
		panic(infoerr)
	}
	var errorlog, err = os.Create(basepath + "\\log_error.log")
	if err != nil {
		panic(err)
	}
	InfoLog = log.New(infolog, "", log.LstdFlags|log.Lshortfile)
	ErrorLog = log.New(errorlog,"", log.LstdFlags|log.Lshortfile)
	InfoLog.Println("LogInfo initiated check in basepathe  : " + basepath)
	ErrorLog.Println("ErrorLog initiated check in basepathe  : " + basepath)
}