package v2

import (
	sqldirect "./sqldirect"
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	apiv2 "./apiv2"
	//beanvalidation "./beanvalidation"
	"strconv"
	"time"
	"sync"
	//mocktest "./mocktest"
	logurslog "./logurslog"
	viperconfig "./viperconfig"
	"github.com/spf13/viper"
	"os"
	oAuth2 "./oAuth2"
	
	//"flag"
)

type SafeCounter struct {
	v map[string] int
	mux sync.Mutex
}




func Submain()  {
	//EchoAPITesting()
	//LoadConfig()
	//EchoAPITesting()
	OauthValidate();
}

func OauthValidate(){
	oAuth2.Oauth2();
}




var (
	configFile string
)


func LoadConfig()  {
	fmt.Println("====os args=====", os.Args)
	configFile := os.Args[0]
	fmt.Println("=====configFile==os=====", configFile)
	viperconfig.LoadConfig()
	fmt.Println("=====usernmane key ====", viper.Get("Username"))
}


func  loggerWriteConcurency()  {
	for k :=0; k <100 ; k++ {
		//fmt.Println(strconv.Itoa(i)+" count value...")
		go WriteInfoLog(k)
		go WriteErrorLog(k)
		// go func (k int)  {
		// 	for i := 0; i <20 ; i++ {
		// 		fmt.Println(strconv.Itoa(k)+"..thread running.."+strconv.Itoa(i))
		// 		//logurslog.InfoLog(strconv.Itoa(k)+" test info message"+strconv.Itoa(i))	
		// 		//time.Sleep(time.Second * 1)
		// 	}
		// }(k)
	
	}
	var input string
  fmt.Scanln(&input)

}

func WriteInfoLog(k int)  {
	for i := 0; i <20 ; i++ {
		//fmt.Println(strconv.Itoa(k)+"..thread running.."+strconv.Itoa(i))
		logurslog.InfoLog(strconv.Itoa(k)+"<<== Main thread childThread==>>"+strconv.Itoa(i))	
		//time.Sleep(time.Second * 1)
	}
}

func WriteErrorLog(k int)  {
	for i := 0; i <20 ; i++ {
		//fmt.Println(strconv.Itoa(k)+"..error thread running.."+strconv.Itoa(i))
		logurslog.ErorLog(strconv.Itoa(k)+"<<== Main thread childThread==>>"+strconv.Itoa(i))
		time.Sleep(time.Second * 1)
	}
}

func TestMutex(){
	m := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go m.Inc("somkey"+ strconv.Itoa(i))
		//time.Sleep(time.Second)
	}
	fmt.Println(m.v)
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	c.mux.Lock()
	// Lock so only one goroutine at a time can access the map c.v.
	fmt.Println("map writing.....", key)
	c.v[key]++
	c.mux.Unlock()
}


func EchoAPITesting(){
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	   }))

	//e.Use(beanvalidation.Beanvalidatoins)
	e.POST("/login", apiv2.Login)
	e.GET("/getpatients", apiv2.GetPatients)
	// e.GET("/ws/:username", api.HandleConnections)
	//  e.GET("/callws/:username", api.WebScoketTest)  
	//  e.GET("/getliveusers", api.GetLiveUsers)
	//  e.POST("/uploadfile", api.UploadFile)
	// fmt.Println("server going start.......")
	fmt.Println("initiating logs.......")
	//logger.Init()
	//WebSocketListen()
	e.Logger.Fatal(e.Start("192.168.8.146:8081"))   
	
}

func GetUserDetails(){
	useDetails, err := sqldirect.CheckUserDetails("p3@p.com","fdrf")
	if err != nil {
		//panic(err)
		fmt.Println(" error while getting user details", err)
	}else{
		fmt.Println(" User details ....", useDetails)
	}

}

func TestDbConnection(){	
	fmt.Println(" sub main test")
	db,err := sqldirect.GetDBConnection()
	if err != nil {
		fmt.Println(" error in db connection ",err)
	}else{
		fmt.Println(" db creation done ",db)
	}
}