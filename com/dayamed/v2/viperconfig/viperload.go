package viperconfig

import(
	"github.com/spf13/viper"
	"fmt"
	"github.com/fsnotify/fsnotify"
	
)

var filePath string = "C:\\go_workspace\\golang-rajesh"

func LoadConfig() {
	fmt.Println(" loading config params from json file........")
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("json") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(filePath)
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}else{
		fmt.Println("==username======", viper.Get("Username"))
		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			fmt.Println("Config file changed:", e.Name)
		})
	}
}

func LoadConfigWithReturn() (viper.Viper,error) {
	fmt.Println(" loading config params from json file........")
	//viper := new(viper)
	viper := new(viper.Viper)
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("json") // REQUIRED if the config file does not have the extension in the name
	//viper.AddConfigPath("/etc/appname/")   // path to look for the config file in
	//viper.AddConfigPath("$HOME/go_workspace")  // call multiple times to add many search paths
	viper.AddConfigPath(filePath)
	viper.AddConfigPath(".")               // optionally look for config in the working directory
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
		//return nil,err
	}else{
		fmt.Println("==username======", viper.Get("Username"))
		viper.WatchConfig()
		viper.OnConfigChange(func(e fsnotify.Event) {
			fmt.Println("Config file changed:", e.Name)
		})
		return *viper,nil
	}
}


