package apiv2

import (
		"encoding/json"
		"fmt"
		"net/http"
		structs "../../structs"
		jwttoken "../../jwttoken"
		"github.com/labstack/echo"
		sqldirect "../sqldirect"
		"github.com/spf13/viper"
		
)



	var credentialArray =  make(map[int] *structs.Credentials)
 var i int = 0;
func Login(c echo.Context) error{
	var creds structs.Credentials
	defer c.Request().Body.Close()
	fmt.Println(" config param in login API.....", viper.Get("Username"))
	fmt.Println("c.Request().Body.......", c.Request().Body)
	err := json.NewDecoder(c.Request().Body).Decode(&creds)
	fmt.Println("c.Request().Body. after......", c.Request().Body)
	//err := c.Bind(&creds)
	fmt.Println("V2 creds.Username", creds.Username)
	fmt.Println("creds.Password", err)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error)
	}
	credentialArray[i]= &creds
	i++
	
	jwtTokenValue := jwttoken.JwtGenerater(creds)
	userDetails, err := sqldirect.CheckUserDetails(creds.Username, creds.Password)
	if err != nil {
		loginResponse := &structs.LoginResponse{Status: "Fail", Message: "Error while getting userDetails", JwtToken: jwtTokenValue, Username:  creds.Username }
		return c.JSON(http.StatusOK, loginResponse)
	}
	fmt.Println("=userDetails return from do layer ===>>===",userDetails)
	if (structs.UserDetails{}) == userDetails {
			loginResponse := &structs.LoginResponse{Status: "Fail", Message: "Invalid User details", JwtToken: jwtTokenValue, Username:  creds.Username }
			return c.JSON(http.StatusOK, loginResponse)
		}else {
			loginResponse := &structs.LoginResponse{JwtToken: jwtTokenValue, Username:  creds.Username }
			return c.JSON(http.StatusOK, loginResponse)
		}
}

func GetLiveUsers(c echo.Context) error  {
	return c.JSON(http.StatusOK, credentialArray)
}

func GetPatients(c echo.Context) error {
	patientDetailsAry := []structs.Patient{ structs.Patient{Id: "5",Name: "Rajesh",Phonenumber:"12444333333", EmailId:"rajaj@l.com"},
	structs.Patient{Id: "7", Name: "xxxx",Phonenumber: "09878998899", EmailId:"a@a.com"}}
	return c.JSON(http.StatusOK, patientDetailsAry)
}