package sqldirect

import (
	structs "../../structs"
	"errors"
	"fmt"
	"database/sql"
)

func CheckUserDetails(username string, password string)  (structs.UserDetails, error)  {
	db,err := GetDBConnection()
	userDetailsStruct := structs.UserDetails{}
	if err != nil {
		fmt.Println(" error in db connection ",err)
		return userDetailsStruct,errors.New("error while creating in db connection..")
	}
	stmt, err := db.Prepare("select user_id,gender,role,email_id,first_name,mobilenumber from user_details where email_id= ? and password = ?")
	if err != nil {
		fmt.Println("stement error..",err)
		return userDetailsStruct,errors.New("statement error")
	}
	err = stmt.QueryRow(username, password).Scan(&userDetailsStruct.User_id,&userDetailsStruct.Gender,&userDetailsStruct.Role,&userDetailsStruct.Email_id,&userDetailsStruct.First_name,&userDetailsStruct.Mobilenumber)
	if err != nil {
		if err == sql.ErrNoRows {
				fmt.Println("No Rows in result",err)
				return userDetailsStruct,errors.New("No Rows in result")
			}else{
				fmt.Println("stement error..",err)
				return userDetailsStruct,errors.New("Error while execute the statement..")
			}
	}
	return userDetailsStruct,nil
}