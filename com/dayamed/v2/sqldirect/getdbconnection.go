package sqldirect

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"errors"
	"time"
)

func GetDBConnection() (db*sql.DB,  errmsg error) {
	fmt.Println("getting db connection....")
	//db, err := sql.Open("mysql","root:root@tcp(192.168.8.146:3306)/godb")
	db, err := sql.Open("mysql","root:root@tcp(localhost:3306)/godb")
	if err != nil{
		fmt.Println("error while creating in db connection...")
		errmsg :=  errors.New("error while creating in db connection..")
		return nil,errmsg
	}
		errDb:=db.Ping()
			if err!=nil{
				errmsg :=  errors.New(" db not connection..")
				fmt.Println(errDb)
				return nil,errmsg
			}
	//defer db.Close()
	return db, nil
}

//connection pool implemented
func GetDBConnectionFromPool() (db*sql.DB){
    // Initialise a new connection pool
    //db, err := sql.Open("postgres", "postgres://user:pass@localhost/db")
    db, err := sql.Open("mysql","root:root@tcp(localhost:3306)/godb")

    //added with db host
    //db, err := gorm.Open("mysql", "root:root@(192.168.8.146:3306)/godb")

    //db, err := gorm.Open("mysql", "root:root@(192.168.1.86:3306)/godb?charset=utf8&parseTime=True&loc=Local")
    if err != nil {
        panic(err.Error())
     }
    // db.Exec("PRAGMA key = auxten;")
    // Migrate the schema
	//db.AutoMigrate(&structs.UserDetails{})
    // SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
    db.SetMaxIdleConns(15)

    // SetMaxOpenConns sets the maximum number of open connections to the database.
    db.SetMaxOpenConns(25)

    // SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
    db.SetConnMaxLifetime(5 * time.Second)

	return db
}