package logurslog

import (
	"github.com/Sirupsen/logrus"
	"os"
	"io"
	"fmt"
)

var infoFile, errorFile *os.File;
//var infologger, errorloggers logs;
var infoLogger = logrus.New()
var errorLogger = logrus.New()

func InitLog(){
	var filename string = "loginfo.log"
	// Create the log file if doesn't exist. And append to it if it already exists.
	infoFile, err := os.OpenFile(filename, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
	if err != nil {
		panic("error while init info logs")
	}
	infoLogger.SetFormatter(GetFormatter())
	infoLogger.SetOutput(io.MultiWriter(os.Stderr, infoFile))

	//error log
	var errorfilename string = "logerror.log"
	// Create the log file if doesn't exist. And append to it if it already exists.
	errorFile, err := os.OpenFile(errorfilename, os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644)
	if err != nil {
		panic("error while init error logs")
	}
	errorLogger.SetFormatter(GetFormatter())
	errorLogger.SetOutput(io.MultiWriter(os.Stderr, errorFile))
}	
	
func InfoLog(msg string){
	fmt.Println("info logs to write....."+msg)
	
	infoLogger.Info(msg)
    //log.Error("Not fatal. An error. Won't stop execution")
}

func ErorLog(msg string){
	fmt.Println("error logs to write....."+msg)
	errorLogger.Error(msg)
}

func GetFormatter() logrus.Formatter{
	Formatter := new(logrus.TextFormatter)
    Formatter.TimestampFormat = "02-01-2006 15:04:05"
	Formatter.FullTimestamp = true
	return Formatter
}