package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"
	"log"
	"encoding/hex"
	"fmt"
)




func ExampleNewGCM_encrypt() {
	//CIPHER_KEY := []byte("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBAcC5jb20iLCJleHAiOjE1NzM1NjYyOTd9.eZWPKbDgHrFaVdV002-dvTYC37Z5-0NQKmGSUvp1lOo")
	CIPHER_KEY := "6368616e676520746869732070617373"
	
	msg := "p@p.com"
	
	if encrypted, err := encrypt(CIPHER_KEY, msg); err != nil {
		log.Println(err)
	} else {
		log.Printf("CIPHER KEY: %s\n", string(CIPHER_KEY))

		log.Printf("ENCRYPTED: %s\n", encrypted)

		if decrypted, err := decrypt(CIPHER_KEY, encrypted); err != nil {
			log.Println(err)
		} else {
			log.Printf("DECRYPTED: %s\n", decrypted)
		}
	}
}

func encrypt(CIPHER_KEY string, message string) (encmess string, err error) {
	plainText := []byte(message)
	key := []byte(CIPHER_KEY)
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return
	}

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], plainText)
	
	encmess = base64.URLEncoding.EncodeToString(cipherText)
	return
}

func decrypt(CIPHER_KEY string, securemess string) (decodedmess string, err error) {
	key := []byte(CIPHER_KEY)
	cipherText, err := base64.URLEncoding.DecodeString(securemess)
	if err != nil {
		return
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("Ciphertext block size is too short!")
		return
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)

	decodedmess = string(cipherText)
	return
}


func ExampleNewGCM_encryptNew() {
	// Load your secret key from a safe place and reuse it across multiple
	// Seal/Open calls. (Obviously don't use this example key for anything
	// real.) If you want to convert a passphrase to a key, use a suitable
	// package like bcrypt or scrypt.
	// When decoded the key should be 16 bytes (AES-128) or 32 (AES-256).
	//key, _ := hex.DecodeString("6368616e676520746869732070617373776f726420746f206120736563726574")
	//key, _ := hex.DecodeString("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBAcC5jb20iLCJleHAiOjE1NzM3MTQ4ODZ9.n0FjwyR0svtVJ7CKMyyOTCkEHHM3ixKW_s0p6VOfTcU")
	key := []byte("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBAcC5jb20iLCJleHAiOjE1NzM3MTQ4ODZ9.n0FjwyR0svtVJ7CKMyyOTCkEHHM3ixKW_s0p6VOfTcU")
	plaintext := []byte("SenecaGlobal")
	nonce, _ := hex.DecodeString("64a9433eae7ccceee2fc0eda")
	//key := make(keyLarge ,12)
	fmt.Println("key=====",key)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	// nonce := make([]byte, 12)
	// if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
	// 	panic(err.Error())
	// }

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)
	fmt.Printf("%x\n", ciphertext)
	ExampleNewGCM_decryptNew(string(ciphertext))
}

func ExampleNewGCM_decryptNew(ciphertextstr string ) {
	// Load your secret key from a safe place and reuse it across multiple
	// Seal/Open calls. (Obviously don't use this example key for anything
	// real.) If you want to convert a passphrase to a key, use a suitable
	// package like bcrypt or scrypt.
	// When decoded the key should be 16 bytes (AES-128) or 32 (AES-256).
	key, _ := hex.DecodeString("6368616e676520746869732070617373776f726420746f206120736563726574")
	ciphertext, _ := hex.DecodeString(ciphertextstr)
	nonce, _ := hex.DecodeString("64a9433eae7ccceee2fc0eda")

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("%s\n", plaintext)
	// Output: exampleplaintext
}