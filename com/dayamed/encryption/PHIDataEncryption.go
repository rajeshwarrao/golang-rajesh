package encryption

import (
	structs "../structs"
	//"fmt"
	"encoding/json"
	"github.com/mitchellh/mapstructure"
	"errors"
	//"strings"
)

var encryptKeys= [7]string{"Email_id","First_name","Mobilenumber","Gender","Password","Last_name","Role"}

//not in user below method but refernce purpose i keept
func EncryptUserDetailsArray_old(userDetails *[]structs.UserDetails, CIPHER_KEY string) *[]structs.UserDetails {
	//CIPHER_KEY := "6368616e676520746869732070617373"
	arrySize := len(*userDetails)
	var encryptUserDetails []structs.UserDetails 
	encryptUserDetails = make([]structs.UserDetails, arrySize)
	
	userDetailsJsonArray, err := json.Marshal(userDetails)
			if err != nil {
				panic(err)
			}
		// Declared an empty interface of type Array
		var results []map[string]interface{}
		// Unmarshal or Decode the JSON to the interface.
		json.Unmarshal([]byte(string(userDetailsJsonArray)), &results)
			
		for key, result := range results {
			//Reading each value by its key
			for encryptKey :=  range encryptKeys {
				if keyValue, ok := result[encryptKeys[encryptKey]].(string); ok {
					encryptValue, err := encrypt(CIPHER_KEY,keyValue)
					if err != nil {
						panic(err)
					}
					result[encryptKeys[encryptKey]] = encryptValue
				} 
			}	
			userDetailsStruct := structs.UserDetails{}
			mapstructure.Decode(result, &userDetailsStruct)
			encryptUserDetails[key] = userDetailsStruct
		}
		return &encryptUserDetails
}

func EncryptUserDetailsArray(userDetails []structs.UserDetails, CIPHER_KEY string) *[]structs.UserDetails {
	arrySize := len(userDetails)
	var encryptUserDetailsArray []structs.UserDetails 
	encryptUserDetailsArray = make([]structs.UserDetails, arrySize)
		for index, userDetailsRef := range userDetails {
			encryptUserDetails, err := EncryptUserDetails(&userDetailsRef, CIPHER_KEY)
			if err != nil {
				panic(err)
			}
			encryptUserDetailsArray[index] = encryptUserDetails
		}
	return &encryptUserDetailsArray
}



func EncryptUserDetails(userDetails *structs.UserDetails, CIPHER_KEY string) (structs.UserDetails, error) {
	//CIPHER_KEY := "6368616e676520746869732070617373"
	userDetailsJson, err := json.Marshal(userDetails)
	userDetailsStruct := structs.UserDetails{}
			if err != nil {
				panic(err)
				return userDetailsStruct,errors.New("userDetails parsing json error ")
			}
			// Declared an empty interface of type Array
		var results map[string]interface{}
		// Unmarshal or Decode the JSON to the interface.
		json.Unmarshal([]byte(string(userDetailsJson)), &results)

			for encryptKey :=  range encryptKeys {
				if keyValue, ok := results[encryptKeys[encryptKey]].(string); ok {
					dcryptValue, err := encrypt(CIPHER_KEY,keyValue)
					if err != nil {
						panic(err)
						return userDetailsStruct,errors.New("error while encryption of key "+encryptKeys[encryptKey]+" and vlaue "+keyValue)
					}
					results[encryptKeys[encryptKey]] = dcryptValue
				}
			}
		
		mapstructure.Decode(results, &userDetailsStruct)
		return userDetailsStruct, nil
}


func DecryptUserDetails(userDetails *structs.UserDetails, CIPHER_KEY string) *structs.UserDetails {
	//CIPHER_KEY := "6368616e676520746869732070617373"
	userDetailsJson, err := json.Marshal(userDetails)
			if err != nil {
				panic(err)
			}
			// Declared an empty interface of type Array
		var results map[string]interface{}
		// Unmarshal or Decode the JSON to the interface.
		json.Unmarshal([]byte(string(userDetailsJson)), &results)

			for encryptKey :=  range encryptKeys {
				if keyValue, ok := results[encryptKeys[encryptKey]].(string); ok {
					dcryptValue, err := decrypt(CIPHER_KEY,keyValue)
					if err != nil {
						panic(err)
					}
					results[encryptKeys[encryptKey]] = dcryptValue
				}
			}
		userDetailsStruct := structs.UserDetails{}
			mapstructure.Decode(results, &userDetailsStruct)
		return &userDetailsStruct
}