package encryption

import (
	"crypto/aes"
	"crypto/cipher"
	//"encoding/hex"
	"crypto/rand"
	"io"
	"fmt"
)

func ExampleNewGCMEncrypter() {
	// The key argument should be the AES key, either 16 or 32 bytes
	// to select AES-128 or AES-256.
	key := []byte("AES256Key-32Characters1234567890")
	//byte[] testkey := []byte("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBAcC5jb20iLCJleHAiOjE1NzM1NjQxNDV9.5G0oZ7x_T15B5IGcmrgP-xKKc9Z1h6iE2Efym7Z15FQ")
	//key := make([]byte("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InBAcC5jb20iLCJleHAiOjE1NzM1NjQxNDV9.5G0oZ7x_T15B5IGcmrgP-xKKc9Z1h6iE2Efym7Z15FQ"),32)
	plaintext := []byte("exampleplaintext")

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	// Never use more than 2^32 random nonces with a given key because of the risk of a repeat.
	nonce := make([]byte, 12)
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err.Error())
	}
    fmt.Printf("nonce === %x\n", nonce)

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)
	fmt.Printf("%x\n", ciphertext)
	ExampleNewGCMDecrypter(ciphertext, nonce)
}


func ExampleNewGCMDecrypter(ciphertext []byte, nonce [] byte) {
	// The key argument should be the AES key, either 16 or 32 bytes
	// to select AES-128 or AES-256.
	key := []byte("AES256Key-32Characters1234567890")
	//ciphertext, _ := hex.DecodeString("2df87baf86b5073ef1f03e3cc738de75b511400f5465bb0ddeacf47ae4dc267d")

	//nonce, _ := hex.DecodeString("afb8a7579bf971db9f8ceeed")

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		panic(err.Error())
	}

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("%s\n", plaintext)
	// Output: exampleplaintext
}