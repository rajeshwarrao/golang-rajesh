package channelsAsync

import ( "fmt"
"time"
"math/rand"
)

func ChannTest() <-chan int32{
	r := make(chan int32)

	go func(){
		defer close(r)
		fmt.Println("in thread process")
		time.Sleep(time.Second * 5)
		r <- rand.Int31n(100)
	}()
	
	return r
}