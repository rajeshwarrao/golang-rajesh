package orm


import (
	"fmt"
	"net/http"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	

   )

type User struct {
    gorm.Model
    Name  string
	Email string
	id int
	mobilnumber int
}

type UserDetails struct {
    gorm.Model
    Name  string
	Email string
	id int
	mobilnumber int
	address string
	password string
	
}





func allUsers(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "All Users Endpoint Hit")
}

func handelRequest(){
	/*myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/users", getAllUser).Methods("GET")
	myRouter.HandleFunc("/addUser", addUser).Methods("GET")
	myRouter.HandleFunc("/login", login).Methods("POST")
	http.ListenAndServe(":8081", myRouter)
	*/
	//http.HandleFunc("/signin", Signin)
	//http.ListenAndServe(":8081", nil)
	
}

func main223() {
    fmt.Println("Go ORM Tutorial")
	initialMigration()

	fmt.Println("handelRequest")

    // Handle Subsequent requests
    handelRequest()
}


func initialMigration() (db*gorm.DB){
    db, err := gorm.Open("mysql", "root:root@/godb")
    if err != nil {
        fmt.Println(err.Error())
        panic("failed to connect database")
    }
    // Migrate the schema
	db.AutoMigrate(&User{})
	db.AutoMigrate(&UserDetails{})
	//defer db.Close()
	return db
}

func getAllUser(w http.ResponseWriter, r *http.Request) {
	db := initialMigration()
	var  users[] User
	db.Find(&users)
	defer db.Close()
	fmt.Println("users====", users)
	json.NewEncoder(w).Encode(users)

}

func addUser(w http.ResponseWriter, r *http.Request){
	db := initialMigration()
	vars := mux.Vars(r)
	name := vars["name"]
	email := vars["email"]
	fmt.Println("email ne wUser====", name, email)
	db.Create(&User{Name:name, Email:email})
	fmt.Fprintf(w, "New User Successfully Created")
}




// func login(w http.ResponseWriter, r *http.Request){
// 	var creds Credentials;
// 	fmt.Println(" requesst body-=====", r.Body)
// 	err := json.NewDecoder(r.Body).Decode(&creds)
// 	if err != nil {
// 		// If the structure of the body is wrong, return an HTTP error
// 		w.WriteHeader(http.StatusBadRequest)
// 		return
// 	}
// 	fmt.Println("Credentials===",creds.Username, creds.Password);
// 	//fmt.Println("Credentials=====%s!,%s!",creds.username, creds.password)
// 	expirationTime := time.Now().Add(5 * time.Minute)

// 	claims := &Claims{
// 		Username: creds.Username,
// 		StandardClaims: jwt.StandardClaims{
// 			// In JWT, the expiry time is expressed as unix milliseconds
// 			ExpiresAt: expirationTime.Unix(),
// 		},
// 	}

// 	// Declare the token with the algorithm used for signing, and the claims
// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
// 	// Create the JWT string
// 	tokenString, err := token.SignedString(jwtKey)
// 	if err != nil {
// 		// If there is an error in creating the JWT return an internal server error
// 		w.WriteHeader(http.StatusInternalServerError)
// 		return
// 	}

// 	// Finally, we set the client cookie for "token" as the JWT we just generated
// 	// we also set an expiry time which is the same as the token itself
// 	http.SetCookie(w, &http.Cookie{
// 		Name:    "token",
// 		Value:   tokenString,
// 		Expires: expirationTime,
// 	})
	
// }